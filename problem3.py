number = 600851475143 
divider = 2

while (divider * divider < number):
     while (number % divider == 0):
         number = number / divider
     divider = divider + 1

print (number)
