#problem 16  https://projecteuler.net/problem=16
$value =  2
$powVal = 15
$powResult=[math]::pow($value, $powVal )
Write-Host $powResult
Write-Host  ($powResult -split '' | measure -sum).sum
